package interfataUtilizator;


import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import aplicatieBancara.Banca;


public class Start extends JFrame{
	JButton personOp = new JButton("Operatiuni client");
	JButton accountOp= new JButton("Operatiuni cont");
	JButton deposit = new JButton("Operatiuni depozit");
	JButton withdraw = new JButton("Operatiuni retragere");
	private static final long serialVersionUID = 1L;
	GridBagConstraints c = new GridBagConstraints();
	private JPanel pane = new JPanel(new GridBagLayout());
	
	public static Banca banca;
	
	public Start(){
		super("Aplicatie bancara");
		c.fill=GridBagConstraints.HORIZONTAL;
		pane.setBackground (new Color(102,0,153));
		banca=new Banca();
		init();
	}
	
	public void init(){
		
		personOp.setBackground(Color.green);
		personOp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				OperatiuniClient p=new OperatiuniClient();
				p.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				p.setVisible(true);
				p.pack();
			}
		});
			
		accountOp.setBackground(Color.cyan);
		accountOp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				OperatiuniContBancar a=new OperatiuniContBancar();
				a.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				a.setVisible(true);
				a.pack();
			}
		});
		
	
		deposit.setBackground(Color.yellow);
		deposit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				OperatiuniDepozit d=new OperatiuniDepozit();
				d.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				d.setVisible(true);
			}
		});
		
	
		withdraw.setBackground(Color.orange);
		withdraw.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				OperatiuniRetrageri w=new OperatiuniRetrageri();
				w.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				w.setVisible(true);
		
			}
		});
		
		c.gridx = 0;	c.gridy = 0;
		pane.add(personOp);
		c.gridx = 0;	c.gridy = 1;
		pane.add(accountOp);
		c.gridx = 0;	c.gridy = 2;
		pane.add(deposit);
		c.gridx = 0;	c.gridy = 3;
		pane.add(withdraw);
		
		this.add(pane);   
		 this.setLocation(380,310);
	}
	
	public static void main(String[] args){
		JFrame demo = new Start();
		demo.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		demo.pack();
		demo.setVisible(true);
		

		
	}

}
