package interfataUtilizator;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import aplicatieBancara.Banca;
import aplicatieBancara.ContBancar;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;


public class OperatiuniRetrageri extends JFrame{
	private static final long serialVersionUID = 1L;
	private int idH;
	private JComboBox<Integer> idA;
	
	public OperatiuniRetrageri(){
		super("Retrageri");
		setBounds(230, 100, 230, 230);
		init();
	}
	
	public void init(){
		JPanel bigPanel = new JPanel();
		JPanel panelLbl= new JPanel();
		JPanel panel = new JPanel();
		JPanel panelA= new JPanel();
		JPanel panelD= new JPanel();
		JPanel panelBtn= new JPanel();
		
		bigPanel.setBackground(new Color(255,204,153));
		panelLbl.setBackground(new Color(255,204,153));
		panel.setBackground(new Color(255,204,153));
		panelA.setBackground(new Color(255,204,153));
		panelD.setBackground(new Color(255,204,153));
		panelBtn.setBackground(new Color(255,204,153));
		
		JButton setH= new JButton("Alege cont");
		JButton withdraw = new JButton("Retrage");
		JLabel giveId = new JLabel("Id cont: ");
		JLabel giveHolder= new JLabel("Id client:");
		JLabel giveSum= new JLabel("Suma: ");
		JLabel anLbl= new JLabel("Anul retragerii: ");
		JTextField sum = new JTextField(10);
		JTextField an= new JTextField(10);
		
		bigPanel.setLayout(new BoxLayout(bigPanel,BoxLayout.PAGE_AXIS));
		panelLbl.setLayout(new BoxLayout(panelLbl,BoxLayout.PAGE_AXIS));
		panel.setLayout(new BoxLayout(panel,BoxLayout.PAGE_AXIS));
			
		Banca bank= Start.banca;
		int nrClienti=bank.getClients().size(); //se extrage numarul de clienti din banca 
		Integer[] idHolders= new Integer[nrClienti]; //se extrage id-ul fiecarui client din banca 
		for(int i=0;i<nrClienti;i++)
			idHolders[i]=bank.getClients().get(i).getId();
		JComboBox<Integer> idCombo= new JComboBox<>(idHolders); //se creeaza un comboBox cu id-urile clientilor din banca 
		panelD.add(idCombo);
		panelLbl.add(giveHolder);
		panelLbl.add(giveSum);
		panelLbl.add(anLbl);
		panel.add(idCombo);
		panel.add(sum);	
		panel.add(an);
		
		panelA.add(panelLbl);
		panelA.add(panel);
			
		setH.setBackground(new Color(255,229,204));
		
		setH.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(bank.getClients().size()>0){
					Integer idHolder=(int)idCombo.getSelectedItem(); //se extrage id-ul clientului selectat
					int nrConturiTitular=bank.getAccounts(idHolder).size();//se extrage numarul de conturi pe care le are clientul
					Integer[] id = new Integer[nrConturiTitular];//se creeaza un comboBox cu id-urile conturilor clientului ales 
					for(int i=0;i<nrConturiTitular;i++) {
						id[i]=bank.getAccounts(idHolder).get(i).getIdAcc();
					}
					JComboBox<Integer> idAcc= new JComboBox<>(id);
				
					panelD.removeAll();
					panelD.add(giveId);
					panelD.add(idAcc);
				
					saveHolder(idHolder);
					saveCombo(idAcc);
					setContentPane(bigPanel);

				}
				else
					JOptionPane.showMessageDialog(null, "Failed Operation!");
			}
		});
		panelBtn.add(setH);
	
		withdraw.setBackground(new Color(255,229,204));
		withdraw.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int idAcc,init,ok=0;
				ArrayList<ContBancar> accounts;
				ContBancar account;
				
				if(bank.getClients().size()>0){
					idAcc= (int)idA.getSelectedItem(); //se selecteaza contul dorit a se face retragere
				    accounts= bank.getAccounts(idH); //se extrag conturile clientului selectat
					account= null;
					for(ContBancar a: accounts)
						if(a.getIdAcc()==idAcc)
							account=a;//se cauta contul dorit a se insera
					 init = account.getSold();
					if(!sum.getText().isEmpty()){
						account.addObserver(bank.getClient((int)idCombo.getSelectedItem()));//observator pentru contul curent
						if(account.getTip().equals("Spending Account")) {
							account.withdraw(Integer.parseInt(sum.getText()));
							JOptionPane.showMessageDialog(null, "S-a retras din cont!");
						}else {
							
						
						if(Integer.parseInt(sum.getText())<= account.getSold()){
							if(Integer.parseInt(an.getText())>0 && account.getTip().equals("Saving Account")) {
							
							account.deposit((int)(account.getSold()*Integer.parseInt(an.getText())*0.1));
							account.withdraw(Integer.parseInt(sum.getText()));
							JOptionPane.showMessageDialog(null, "S-a retras din cont!");
							}
							else
							{
							if(Integer.parseInt(an.getText())==0 && account.getTip().equals("Saving Account")) {
								
								account.withdraw(Integer.parseInt(sum.getText())+(int)(account.getSold()*0.1));
								JOptionPane.showMessageDialog(null, "S-a retras din cont !");
							}
							else
							{   
								JOptionPane.showMessageDialog(null, "Nu s-au putut retrage bani din cont!");
							}
							}
						}
							
							bank.writeData();
							
						
							}
						}
					}
			}
		
		});
		panelBtn.add(withdraw);
	
		bigPanel.add(panelA);
		bigPanel.add(panelD);
		bigPanel.add(panelBtn);
		setContentPane(bigPanel);
	}
	
	
	private void saveHolder(int idHolder){
		idH=idHolder;
	}
	
	private void saveCombo(JComboBox<Integer> combo){
		idA=combo;
	}
}