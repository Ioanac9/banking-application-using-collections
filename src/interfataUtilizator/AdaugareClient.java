package interfataUtilizator;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import aplicatieBancara.Banca;
import aplicatieBancara.Client;


public class AdaugareClient extends JFrame{
	private static final long serialVersionUID = 1L;
	GridBagConstraints c = new GridBagConstraints();
	private JPanel pane = new JPanel(new GridBagLayout());
	
	JLabel phoneLbl = new JLabel("Telefon: ");
	JLabel mailLbl = new JLabel("Email: ");
	JLabel nameLbl= new JLabel("Nume: ");
	JLabel cnpLbl = new JLabel("CNP: ");
	JLabel idLbl = new JLabel("Id: ");
	
	JTextField phone= new JTextField(15);
	JTextField mail = new JTextField(15);
	JTextField name= new JTextField(15);
	JTextField cnp = new JTextField(15);
	JTextField id = new JTextField(15);
	
	JButton add = new JButton("Adauga Client");
	
	public AdaugareClient(){
		super("Adauga client");
		pane.setBackground(new Color(102,255,102));
		c.fill=GridBagConstraints.HORIZONTAL;
		init();
	}
	
	public void init(){
		
		c.insets = new Insets(10,10,0,0);
		c.gridx = 0;	c.gridy = 0;
		pane.add(idLbl,c);
        idLbl.setForeground(Color.black);
		c.gridx = 1;	c.gridy = 0;
		pane.add(id,c);
		idLbl.setVisible(false);
		id.setVisible(false);
		c.gridx = 0;	c.gridy = 1;
		pane.add(nameLbl,c);
		nameLbl.setForeground(Color.black);
		c.gridx = 1;	c.gridy = 1;
		pane.add(name,c);
		
		c.gridx = 0;	c.gridy = 2;
		pane.add(cnpLbl,c);
		cnpLbl.setForeground(Color.black);
		c.gridx = 1;	c.gridy = 2;
		pane.add(cnp,c);
		
		c.gridx = 0;	c.gridy = 3;
		pane.add(phoneLbl,c);
		phoneLbl.setForeground(Color.black);
		c.gridx = 1;	c.gridy = 3;
		pane.add(phone,c);
		
		c.gridx = 0;	c.gridy = 5;
		pane.add(mailLbl,c);
		mailLbl.setForeground(Color.black);
		c.gridx = 1;	c.gridy = 5;
		pane.add(mail,c);
		
		
		add.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int idp,hashId;
				//verificam daca campurile care trebuie completate nu sunt goale 
				if( !name.getText().isEmpty() && !cnp.getText().isEmpty()
						&& !phone.getText().isEmpty() && !mail.getText().isEmpty()){
					
					Banca bank= Start.banca;
					//se extrage id-ul clientului
					idp= 9;	
					//se creeaza un client cu datele extrase din textField-uri
					Client person = new Client(idp,name.getText(),cnp.getText(),phone.getText(),mail.getText());
					hashId=person.hashCode();
					//se creeaza o persoana "falsa" cu trasaturile identice ale persoanei originale pentru a calcula 
					//id-ul clientului in baza de date a bancii 
					Client persoanaExistenta=bank.getClient(hashId);
					if (persoanaExistenta==null){
					person.setId(hashId);
					bank.addClient(person);
					JOptionPane.showMessageDialog(null, "Persoana adaugata! ");
					}else {
				    JOptionPane.showMessageDialog(null, "Exista clientul cu id-ul "+idp+ "!");
					}
						}
				else
					JOptionPane.showMessageDialog(null, "Persoana nu a fost adaugata!");
			}
		});
		
		c.insets = new Insets(10,0,10,0);
		c.gridx = 0;	c.gridy = 6;
		pane.add(add,c);
		add.setBackground(new Color(0,204,102));
		
		this.add(pane);
		this.setVisible(true);
		this.pack();
		this.setLocation(200,200);
	}

}