package interfataUtilizator;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import aplicatieBancara.Banca;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;

public class EditareContBancar extends JFrame{
	private static final long serialVersionUID = 1L;
	
	public EditareContBancar(){
		setTitle("Editeaza cont");
		setBounds(400, 100, 250, 180);
		init();
	}
	
	public void init(){
		
		JLabel idHolder = new JLabel("id vechi client:");
		JLabel idNewHolder = new JLabel("id nou client:");
		JLabel idAccount = new JLabel("id cont:");
		
		JPanel bigPanel = new JPanel();
		JPanel panelBtn = new JPanel();
		JPanel panelLbl = new JPanel();
		JPanel panelText = new JPanel();
		JPanel panel = new JPanel();
	    
		JButton edit = new JButton("editeaza");
		
		panelLbl.setLayout(new BoxLayout(panelLbl,BoxLayout.PAGE_AXIS));
		panelText.setLayout(new BoxLayout(panelText,BoxLayout.PAGE_AXIS));
		panel.setLayout(new BoxLayout(panel,BoxLayout.PAGE_AXIS));

		Banca bank= Start.banca;
		panelLbl.add(idAccount);
		panelLbl.add(idHolder);
		panelLbl.add(idNewHolder);
		//aceasta editare reprezinta mai mult o schmbare a proprietarilor unui cont 
		//se alege un cont de la un client si i se schimba proprietarul 
		
		Integer[] idHolders= new Integer[bank.getClients().size()];
		for(int i=0;i<bank.getClients().size();i++)
			idHolders[i]=bank.getClients().get(i).getId(); 

		JComboBox<Integer> idHolderNC= new JComboBox<>(idHolders);//comboBox-ul care va reprezenta id-ul noului proprietar
		JComboBox<Integer> idHolderC= new JComboBox<>(idHolders); //comboBox-ul care reprezinta vechiul id al proprietarului
	
		Integer[] idAccounts= new Integer[bank.getAllAccounts().size()]; //se extrag id-urile tututor conturilor si se 
		int nrConturi=bank.getAllAccounts().size();                     //construieste un ComboBox din ele 
		for(int i=0;i<nrConturi;i++)
			idAccounts[i]=bank.getAllAccounts().get(i).getIdAcc();
		JComboBox<Integer> idAccountsC= new JComboBox<>(idAccounts);
		
		panelText.add(idAccountsC);
		panelText.add(idHolderC);
		panelText.add(idHolderNC);
	
	
		edit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int idNH,idH,idA;
				if(bank.getClients().size()>0 && bank.getAllAccounts().size()>0){
					
					idNH= (int)idHolderNC.getSelectedItem();
					idH = (int)idHolderC.getSelectedItem();	
					idA = (int)idAccountsC.getSelectedItem();
					bank.editCont(idH, idA, idNH);
					JOptionPane.showMessageDialog(null, "Cont editat!");
				}
				else
					JOptionPane.showMessageDialog(null, "Failed Operation!");
			}
		});
	
		bigPanel.add(panelLbl);
		bigPanel.add(panelText);
		panelBtn.add(edit);
		bigPanel.add(panelBtn);	
		panel.add(bigPanel);
		
		setContentPane(panel);
		setVisible(true);
	}
}
