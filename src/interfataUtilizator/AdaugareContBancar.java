package interfataUtilizator;


import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import aplicatieBancara.Banca;
import aplicatieBancara.Client;
import aplicatieBancara.ContBancar;
import aplicatieBancara.ContBancarCheltuieli;
import aplicatieBancara.ContBancarEconomii;


public class AdaugareContBancar extends JFrame {
	private JPanel pane = new JPanel(new GridBagLayout());
	private static final long serialVersionUID = 1L;
	GridBagConstraints c = new GridBagConstraints();
	
	JLabel idHolderLbl= new JLabel("Id client: ");
	JLabel typeLbl = new JLabel("Tip cont: ");
	JLabel balanceLbl = new JLabel("Suma: ");
	JLabel idLbl = new JLabel("Id cont: ");
	
	JTextField idHolder= new JTextField(20);
	JTextField balance = new JTextField(20);
	JTextField idAcc = new JTextField(20);
	
	
	JButton add = new JButton("Adauga");
	
	public AdaugareContBancar(){
		super("Adauga cont");
		c.fill=GridBagConstraints.HORIZONTAL;
		init();
	}
	
	public void init(){
		String[] type = new String[2];
		type[0]="Saving Account";
		type[1]="Spending Account";
		JComboBox<String> idCombo= new JComboBox<>(type);
		
		Banca bank= Start.banca;
		int nrClienti=bank.getClients().size();
		Integer[] idHolders= new Integer[nrClienti]; 
		
		idAcc.setVisible(false);
		idLbl.setVisible(false);
		for(int i=0;i<nrClienti;i++)
			idHolders[i]=bank.getClients().get(i).getId();//se extrag id-urile tututror clientilor din hashMap
		JComboBox<Integer> idComboClienti= new JComboBox<>(idHolders);//se creeaza un combo cu toate id-urile extrase
	
		add.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int id,idH;
				Integer bl;
				if( !balance.getText().isEmpty()){
			
				 Integer idClient=(int)idComboClienti.getSelectedItem(); //se extrage id-ul clientului selectat
				 
					String type= (String)idCombo.getSelectedItem();      //se extrage tipul contului dorit
					bl= Integer.parseInt(balance.getText());             //se extrage suma de bani intiala din cont
					id= 9;               //se extrage id-ul contului 
					idH=idClient;
					
					if(type.equals("Saving Account")){
						int idcurent,idcont;
						Banca bank= Start.banca;
						Client person= bank.getClient(idH); //se extrag informatiile despre clientul cu id-ul selectat
						if(person!=null){
							ContBancar accountAux= new ContBancarEconomii(id,idH,bl,type);
							idcont=accountAux.getIdAcc();

							if(idcont!=0) {
							idcurent=accountAux.hashCode(); //se calculeaza id-ul contului folosindu-se functia de hash
							ContBancar account= new ContBancarEconomii(idcurent,idH,bl,type);
							bank.addAccount(person, account);
							JOptionPane.showMessageDialog(null, "Cont adaugat!");
						}
						}
						else {
							JOptionPane.showMessageDialog(null, "Nu s-a putut adauga contul!");
						}
					}
					else
					{int idcont,idcurent;
						Banca bank= Start.banca;
					Client person= bank.getClient(idH);
					if(person!=null){
						ContBancar accountAux= new ContBancarCheltuieli(id,idH,bl,"Spending Account");
						idcont=accountAux.getIdAcc();
						
						if(idcont!=0) {
						idcurent=accountAux.hashCode();
						ContBancar account= new ContBancarCheltuieli(idcurent,idH,bl,"Spending Account");
						bank.addAccount(person, account);
						JOptionPane.showMessageDialog(null, "Account Added! ");
					}
					}
						
				else
					{JOptionPane.showMessageDialog(null, "Failed Operation!");
					}}}}
				
		});
		
		c.insets = new Insets(10,10,0,0);
		c.gridx = 0;	c.gridy = 0;
		pane.add(idLbl,c);

		c.gridx = 1;	c.gridy = 0;
		pane.add(idAcc,c);
		
		c.gridx = 0;	c.gridy = 1;
		pane.add(idHolderLbl,c);
		c.gridx = 1;	c.gridy = 1;
		pane.add(idComboClienti,c);
		
		c.gridx = 0;	c.gridy = 2;
		pane.add(balanceLbl,c);
		c.gridx = 1;	c.gridy = 2;
		pane.add(balance,c);
		
		c.gridx = 0;	c.gridy = 3;
		pane.add(typeLbl,c);
		c.gridx = 1;	c.gridy = 3;
		pane.add(idCombo,c);
		
		c.insets = new Insets(10,0,10,0);
		c.gridx = 0;	c.gridy = 4;
		pane.add(add,c);
		
		this.add(pane);
		this.setVisible(true);
		this.pack();
		this.setLocation(200,200);
		
		
	}
}