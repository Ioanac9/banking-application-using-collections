package interfataUtilizator;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class OperatiuniClient extends JFrame{
	private static final long serialVersionUID = 1L;
	GridBagConstraints c = new GridBagConstraints();
	private JPanel pane = new JPanel(new GridBagLayout());
	JButton addC = new JButton("Adauga client");
	JButton deleteC = new JButton("Sterge client");
	JButton editC = new JButton("Editeaza client");
	JButton viewC = new JButton("Afisare clienti");

	public OperatiuniClient(){
		super("Operatiuni clienti");
		c.fill=GridBagConstraints.HORIZONTAL;
		pane.setBackground(new Color(76,153,0));
		init();
	}
	
	public void init(){
			
		addC.setBackground(new Color(51,51,0));
		addC.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AdaugareClient aP=new AdaugareClient();
				aP.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				setVisible(false);
				dispose();
			}
		});
		
		
		deleteC.setBackground(new Color(51,51,0));
		deleteC.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new StergereClient();
				setVisible(false);
				dispose();
			}
		});
	
		
		editC.setBackground(new Color(51,51,0));
		editC.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new EditareClient();
				setVisible(false);
				dispose();
			}
		});
	
			
		viewC.setBackground(new Color(51,51,0));
		viewC.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new AfisareClienti();
				setVisible(false);
				dispose();
			}
		});
		
		
		c.gridx = 0;	c.gridy = 0;
		pane.add(addC,c);
		addC.setForeground(Color.white);
		c.insets = new Insets(10,10,10,0);
		c.gridx = 1;	c.gridy = 0;
		pane.add(deleteC,c);
		deleteC.setForeground(Color.white);
		c.gridx = 2;	c.gridy = 0;
		pane.add(editC,c);
		editC.setForeground(Color.white);
		
		c.insets = new Insets(10,20,10,10);
		c.gridx = 1;	c.gridy = 3;
		pane.add(viewC,c);
		viewC.setForeground(Color.white);
		
		c.gridx = 0;	c.gridy = 4;
		JLabel img;
		ImageIcon folder= new ImageIcon("clientDEAL.jpg");
		img= new JLabel(folder);
		pane.add(img);
	
		this.add(pane);   
		this.setLocation(200,310);
		
	}
}

