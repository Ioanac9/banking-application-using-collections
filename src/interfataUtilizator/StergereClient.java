package interfataUtilizator;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import aplicatieBancara.Banca;
import aplicatieBancara.Client;


public class StergereClient extends JFrame{
	private static final long serialVersionUID = 1L;
	GridBagConstraints c = new GridBagConstraints();
	JLabel giveId = new JLabel("Id client: ");
	private JPanel pane = new JPanel(new GridBagLayout());
	JButton delete = new JButton("Sterge");

	
	public StergereClient(){
		super("Sterge client");
		c.fill=GridBagConstraints.HORIZONTAL;
		pane.setBackground(new Color(0,102,51));
		init();
	}
	
	public void init(){
		
		Banca bank= Start.banca;
		int nrClienti=bank.getClients().size();
		Integer[] ids= new Integer[nrClienti];
		for(int i=0;i<nrClienti;i++) {
			ids[i]=bank.getClients().get(i).getId();
		}
		JComboBox<Integer> idCombo= new JComboBox<>(ids);
		
		delete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(bank.getClients().size()>0){
					Client person= bank.getClient((Integer)idCombo.getSelectedItem());
					bank.stergeClient(person);
					JOptionPane.showMessageDialog(null, "Client sters! ");
				}
				else
					JOptionPane.showMessageDialog(null, "Clientul nu s-a putut sterge! ");
			}
		});
		c.insets = new Insets(10,10,10,10);
		c.gridx = 0;	c.gridy = 0;
		pane.add(giveId,c);
		giveId.setForeground(Color.white);
		c.gridx = 1;	c.gridy = 0;
		pane.add(idCombo,c);
		c.gridx = 0;	c.gridy = 1;
		pane.add(delete,c);
		delete.setBackground(new Color(51,255,153));
		
		this.add(pane);
		this.setVisible(true);
		this.setSize(210,160);
		this.setLocation(200,200);
	}

}