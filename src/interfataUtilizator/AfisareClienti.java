package interfataUtilizator;

import java.awt.Color;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import aplicatieBancara.Banca;


public class AfisareClienti extends JFrame{
	private static final long serialVersionUID = 1L;
	JPanel panel= new JPanel();
	public AfisareClienti(){
		super("Clientii bancii");
		setBounds(400, 100, 600, 480);
		panel.setBackground(new Color(102,0,51));
		init();
	}
	
	public void init(){
				Banca bank= Start.banca;  
				if(bank.getClients().size()>0){
					ArrayList<Object>lists= new ArrayList<Object>();
					lists.addAll(bank.getClients());//se adauga toti clientii bancii
				
					JTable table= bank.createTable(lists);//se creeaza un tabel cu acestia 
					table.setForeground(Color.blue);
				    table.getTableHeader().setBackground(new Color(255,255,102));
				    
					JScrollPane scroll = new JScrollPane(table);
					panel.add(table.getTableHeader());
					panel.add(scroll);
				
					setContentPane(panel);
				}
				else
					JOptionPane.showMessageDialog(null, "Failed Operation!");
		
		setContentPane(panel);
		setVisible(true);
	}

}