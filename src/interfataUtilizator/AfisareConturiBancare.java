package interfataUtilizator;

import java.awt.Color;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import aplicatieBancara.Banca;

public class AfisareConturiBancare extends JFrame{
	private static final long serialVersionUID = 1L; 
	JPanel panel= new JPanel();

	public AfisareConturiBancare(){
		super("Conturile bancii");
		setBounds(400, 100, 600, 480);
		panel.setBackground(new Color(51,0,0));
		init();
	}
	
	public void init(){
		
				Banca bank= Start.banca; 
				if(bank.getAllAccounts().size()>0){
					ArrayList<Object> list= new ArrayList<Object>();
					list.addAll(bank.getAllAccounts());//se extrag toate conturile bancii
				
					JTable table= bank.createTable(list);//se creeaza un tabel cu acestea 
					 table.setForeground(new Color(0,102,51));
					 table.getTableHeader().setBackground(new Color(204,153,255));
					JScrollPane scroll = new JScrollPane(table);
					panel.add(table.getTableHeader());
					panel.add(scroll);
				
					setContentPane(panel);
				}
				else
					JOptionPane.showMessageDialog(null, "Failed Operation!");
		
		setContentPane(panel);
		setVisible(true);
	}

}
