package interfataUtilizator;


import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import aplicatieBancara.Banca;

public class EditareClient extends JFrame{
	private static final long serialVersionUID = 1L;
	GridBagConstraints c = new GridBagConstraints();
	private JPanel pane = new JPanel(new GridBagLayout());
	JLabel giveId = new JLabel("Id client: ");
	JLabel nameLbl = new JLabel("Noul nume: ");
	JLabel phoneLbl = new JLabel("Noul numar telefon: ");
	JLabel mailLbl= new JLabel("Noul email: ");
	JTextField name= new JTextField(20);
	JTextField phone= new JTextField(20);
	JTextField mail= new JTextField(20);
	JButton edit = new JButton("Editeaza");
	
	public EditareClient(){
		super("Editeaza client");
		c.fill=GridBagConstraints.HORIZONTAL;
		pane.setBackground(new Color(102,255,102));
		init();
	}
	
	public void init(){
			
		Banca bank= Start.banca;
		int nrClienti=bank.getClients().size(); // se extrage numarul de clienti din banca 
		
		Integer[] idHolders= new Integer[nrClienti];

		for(int i=0;i<nrClienti;i++)
			idHolders[i]=bank.getClients().get(i).getId(); //se extrag id-urile fiecarui client din banca 
		JComboBox<Integer> idCombo= new JComboBox<>(idHolders);//se creeaza un ComboBox cu ele 
		
		edit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int id;
				if(bank.getClients().size()>0 && !name.getText().isEmpty() && !phone.getText().isEmpty() &&
						 !mail.getText().isEmpty()){
					id= (int)idCombo.getSelectedItem();//se extrage id-ul selectat
					bank.editClient(bank.getClient(id), name.getText(), phone.getText(), mail.getText());
					JOptionPane.showMessageDialog(null, "Client editat! ");
				}
				else
					JOptionPane.showMessageDialog(null, "Nu s-a putut realiza editarea !");
			}
		});
		c.insets = new Insets(10,10,0,0);
		c.gridx = 0;	c.gridy = 0;
		pane.add(giveId,c);
		giveId.setForeground(Color.black);

		c.gridx = 1;	c.gridy = 0;
		pane.add(idCombo,c);
		
		c.gridx = 0;	c.gridy = 1;
		pane.add(nameLbl,c);
		nameLbl.setForeground(Color.black);
		c.gridx = 1;	c.gridy = 1;
		pane.add(name,c);
		
		c.gridx = 0;	c.gridy = 3;
		pane.add(phoneLbl,c);
		phoneLbl.setForeground(Color.black);
		c.gridx = 1;	c.gridy = 3;
		pane.add(phone,c);
		
		c.gridx = 0;	c.gridy = 5;
		pane.add(mailLbl,c);
		mailLbl.setForeground(Color.black);
		c.gridx = 1;	c.gridy = 5;
		pane.add(mail,c);
		
		c.insets = new Insets(10,0,10,0);
		c.gridx = 0;	c.gridy = 6;
		pane.add(edit,c);
		edit.setBackground(new Color(0,204,102));
		this.add(pane);
		this.setVisible(true);
		this.pack();
		this.setLocation(200,200);
	}
	
}
