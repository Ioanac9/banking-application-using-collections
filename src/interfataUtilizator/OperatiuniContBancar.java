package interfataUtilizator;


import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class OperatiuniContBancar extends JFrame {
	private static final long serialVersionUID = 1L;
	GridBagConstraints c = new GridBagConstraints();
	private JPanel pane = new JPanel(new GridBagLayout());
	JButton addC = new JButton("Adauga cont");
	JButton deleteC = new JButton("Sterge cont");
	JButton editC = new JButton("Editeaza cont");
	JButton viewC = new JButton("Afiseaza conturile");
	
	public OperatiuniContBancar(){
		super("Operatiuni cont");
		c.fill=GridBagConstraints.HORIZONTAL;
		pane.setBackground(new Color(0,51,102));
		init();
	}
	
	public void init(){
		
		addC.setBackground(new Color(255,204,229));
		deleteC.setBackground(new Color(255,204,229));
		editC.setBackground(new Color(255,204,229));
		viewC.setBackground(new Color(255,204,229));
		
		addC.addActionListener(new ActionListener() {
			@SuppressWarnings("unused")
			public void actionPerformed(ActionEvent arg0) {
				AdaugareContBancar adaugaCont=new AdaugareContBancar();
				setVisible(false);
			}
		});
		
		deleteC.setBackground(new Color(255,204,229));
		deleteC.addActionListener(new ActionListener() {
			@SuppressWarnings("unused")
			public void actionPerformed(ActionEvent arg0) {
			     StergereContBancar stergeCont=new StergereContBancar();
				setVisible(false);
			}
		});

		editC.addActionListener(new ActionListener() {
			@SuppressWarnings("unused")
			public void actionPerformed(ActionEvent arg0) {
				EditareContBancar editeazaCont=new EditareContBancar();
				setVisible(false);
			}
		});
		
		viewC.addActionListener(new ActionListener() {
			@SuppressWarnings("unused")
			public void actionPerformed(ActionEvent arg0) {
				 AfisareConturiBancare afisareConturi=new AfisareConturiBancare();
				setVisible(false);		
			}
		});

	
		c.gridx = 0;	c.gridy = 0;
		pane.add(addC,c);
		addC.setBackground(new Color(178,192,255));
		c.insets = new Insets(10,10,10,0);
		c.gridx = 1;	c.gridy = 0;
		pane.add(deleteC,c);
		deleteC.setBackground(new Color(178,192,255));
		c.gridx = 2;	c.gridy = 0;
		pane.add(editC,c);
		editC.setBackground(new Color(178,192,255));
		
		c.insets = new Insets(10,20,10,10);
		c.gridx = 1;	c.gridy = 3;
		pane.add(viewC,c);
		viewC.setBackground(new Color(178,192,255));
	    
		c.gridx = 0;	c.gridy = 4;
		JLabel img;
		ImageIcon folder= new ImageIcon("banca.jpg");
		img= new JLabel(folder);
		pane.add(img);
		
		this.add(pane);   
		this.setLocation(240,310);
	}
}