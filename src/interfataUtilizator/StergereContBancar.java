package interfataUtilizator;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import aplicatieBancara.Banca;

public class StergereContBancar extends JFrame{
	private static final long serialVersionUID = 1L;
	private int idH;
	private JComboBox<Integer> idA;

	public StergereContBancar(){
		super("Sterge cont");
		setBounds(300, 100, 250, 200);
		init();
	}
	
	public void init(){
		JPanel bigPanel = new JPanel();
		JPanel panel = new JPanel();
		JPanel panelD= new JPanel();
		JPanel panelBtn= new JPanel();
		
		JLabel giveId = new JLabel("Id cont:");
		JLabel giveHolder= new JLabel("Id client:");
		
		JButton delete = new JButton("Sterge");
		JButton setH= new JButton("Alege cont");

		bigPanel.setLayout(new BoxLayout(bigPanel,BoxLayout.PAGE_AXIS));

		Banca bank= Start.banca;
		int nrClienti=bank.getClients().size();
		Integer[] idHolders= new Integer[nrClienti];
		for(int i=0;i<nrClienti;i++)
			idHolders[i]=bank.getClients().get(i).getId();
		JComboBox<Integer> idCombo= new JComboBox<>(idHolders);
		

		panelD.add(idCombo);
		panel.add(giveHolder);
		panel.add(idCombo);
	
		
		setH.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(bank.getClients().size()>0  && bank.getAllAccounts().size()>0 ){
					Integer idHolder=(Integer)idCombo.getSelectedItem();
                    int nrConturiClient=bank.getAccounts(idHolder).size();
					Integer[] id = new Integer[nrConturiClient];
					for(int i=0;i<nrConturiClient;i++)
						id[i]=bank.getAccounts(idHolder).get(i).getIdAcc();
					JComboBox<Integer> idAcc= new JComboBox<>(id);
				
					panelD.removeAll();
					panelD.add(giveId);
					panelD.add(idAcc);
				
					saveHolder(idHolder);
					saveCombo(idAcc);
				
					setContentPane(bigPanel);
				}
				else
					JOptionPane.showMessageDialog(null, "Nu s-a putut seta contul!");
			}
		});
		panelBtn.add(setH);
		
		delete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(bank.getAccounts(idH).size()>0){
					bank.stergeCont((Integer)idA.getSelectedItem(), idH);
					JOptionPane.showMessageDialog(null, "Stergere reusita! ");
				}
				else
					JOptionPane.showMessageDialog(null, "Nu s-a putut sterge!");
			}
		});
		panelBtn.add(delete);
		
		bigPanel.add(panel);
		bigPanel.add(panelD);
		bigPanel.add(panelBtn);
		setContentPane(bigPanel);
		setVisible(true);
	}
	
	private void saveHolder(int idHolder){
		idH=idHolder;
	}
	
	private void saveCombo(JComboBox<Integer> combo){
		idA=combo;
	}
}