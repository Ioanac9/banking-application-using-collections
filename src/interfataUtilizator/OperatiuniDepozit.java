package interfataUtilizator;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import aplicatieBancara.Banca;
import aplicatieBancara.ContBancar;

import javax.swing.BoxLayout;
import javax.swing.JComboBox;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.Color;

public class OperatiuniDepozit extends JFrame{
	private static final long serialVersionUID = 1L;
	private JComboBox<Integer> idA;
	private int idH;
	
	public OperatiuniDepozit(){
		super("Depozit");
		setBounds(230, 100, 230, 230);
		init();
	}
	
	public void init(){
		JPanel bigPanel = new JPanel();
		bigPanel.setBackground(new Color(255,255,153));
		JPanel panelLbl= new JPanel();
		JPanel panel = new JPanel();
		JPanel panelA= new JPanel();
		JPanel panelD= new JPanel();
		JPanel panelBtn= new JPanel();
		
		JButton setH= new JButton("Alege cont");
		JButton deposit = new JButton("Depoziteaza");
		
		JLabel giveId = new JLabel("Id cont:");
		JLabel giveHolder= new JLabel("Id client:");
		JLabel giveSum= new JLabel("Suma:");
		
		JTextField sum = new JTextField(10);
		
		Banca bank= Start.banca;
		int nrClienti=bank.getClients().size();
		Integer[] idHolders= new Integer[nrClienti];
		
		bigPanel.setLayout(new BoxLayout(bigPanel,BoxLayout.PAGE_AXIS));
		panelLbl.setLayout(new BoxLayout(panelLbl,BoxLayout.PAGE_AXIS));
		panel.setLayout(new BoxLayout(panel,BoxLayout.PAGE_AXIS));
			
		for(int i=0;i<nrClienti;i++)
			idHolders[i]=bank.getClients().get(i).getId();
		JComboBox<Integer> idCombo= new JComboBox<>(idHolders);
		
		panelD.add(idCombo);
		panelLbl.setBackground(new Color(255,255,153));
		panelLbl.add(giveHolder);
		panelLbl.add(giveSum);
		panel.add(idCombo);
		panel.add(sum);	
		panelA.add(panelLbl);
		panelA.add(panel);
		
		setH.setBackground(new Color(204,204,0));
		setH.setForeground(Color.black);
		setH.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(bank.getClients().size()>0){
					Integer idHolder=(int)idCombo.getSelectedItem();
					int nrConturiTitular=bank.getAccounts(idHolder).size();
					Integer[] id = new Integer[nrConturiTitular];
					for(int i=0;i<nrConturiTitular;i++)
						id[i]=bank.getAccounts(idHolder).get(i).getIdAcc();
					JComboBox<Integer> idAcc= new JComboBox<>(id);
				
					panelD.removeAll();
					panelD.add(giveId);
					panelD.add(idAcc);
				
					saveHolder(idHolder);
					saveCombo(idAcc);
				
					setContentPane(bigPanel);
					JOptionPane.showMessageDialog(null, "Succesful Operation!");
				}
				else
					JOptionPane.showMessageDialog(null, "Failed Operation!");
			}
		});
		panelBtn.add(setH);
		deposit.setBackground(new Color(204,204,0));
		deposit.setForeground(Color.black);
		deposit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int idAcc;
				float init;
				int ok=0;
				ArrayList<ContBancar> accounts;
				ContBancar account;
				if(bank.getClients().size()>0){
					account= null;
					idAcc= (Integer)idA.getSelectedItem();
					accounts = bank.getAccounts(idH);
					for(ContBancar a: accounts)
						if(a.getIdAcc()==idAcc)
							account=a;
					init = account.getSold();
					if(!sum.getText().isEmpty()){
						account.addObserver(bank.getClient((int)idCombo.getSelectedItem()));
						account.deposit(Integer.parseInt(sum.getText()));
						bank.writeData();
						
						if(init< account.getSold()){
							ok=1;
						}
					}
				}
			
			if(ok==1) {
				JOptionPane.showMessageDialog(null, "Successful Operation!");
		}
			}
		});
		panelBtn.add(deposit);
		
		bigPanel.add(panelA);
		bigPanel.add(panelD);
		bigPanel.add(panelBtn);
		panelA.setBackground(new Color(255,255,153));
		panelD.setBackground(new Color(255,255,153));
		panelBtn.setBackground(new Color(255,255,153));
		setContentPane(bigPanel);
		setVisible(true);
	}
	
	private void saveCombo(JComboBox<Integer> combo){
		idA=combo;
	}
	
	private void saveHolder(int idHolder){
		idH=idHolder;
	}
	
	
}