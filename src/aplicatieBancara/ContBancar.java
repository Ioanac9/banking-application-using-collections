package aplicatieBancara;



import java.util.Observable;
import java.io.Serializable;

@SuppressWarnings("serial")
public abstract class ContBancar extends Observable implements Serializable {//clasa ContBancar este o clasa 
	                                         //"observata" ,de aceea extinde Observable 
	private int idAcc;  
	private int idHolder;
	private int balance;
	private String tip;
	
	public ContBancar(int idAcc, int idHolder, int balance,String tip){
		this.idAcc=idAcc;
		this.idHolder=idHolder;
		this.balance=balance;
		this.setTip(tip);
	}
	
	public void setIdHolder(int idHolder) {
		this.idHolder = idHolder;
	}

	public int getHolder(){
		return idHolder;
	}
	public int getIdAcc() {
		return idAcc;
	}
	
	public int getSold() {
		return balance;
	}

	public abstract void deposit(int suma);
	public abstract void withdraw(int suma);

	public String getTip() {
		return tip;
	}

	public void setTip(String tip) {
		this.tip = tip;
	}
	
}
