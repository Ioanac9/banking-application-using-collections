package aplicatieBancara;



@SuppressWarnings("serial")
public class ContBancarEconomii extends ContBancar {
	
	private int idAcc; 
	private int idHolder;
	private int balance;
	private String tip;
	
	public ContBancarEconomii(int idAcc, int idHolder, int balance,String tip){
		super(idAcc,idHolder,balance,tip);
		this.idAcc=idAcc;
		this.idHolder=idHolder;
		this.balance=balance;
		this.setTip(tip);
	}
	public void setIdHolder(int idHolder) {
		this.idHolder = idHolder;
	}
	
	public int getHolder(){
		return idHolder;
	}

	public int getIdAcc() {
		return idAcc;
	}
	
	public int getSold() {
		return balance;
	}
	

	public void deposit(int suma){
		int initial=balance;
		if(suma>=0)
			balance=balance+suma;
		setChanged();
		notifyObservers(initial);
	}

	public void withdraw(int suma) {
		int initial=balance;
		if(suma>=1000)
			if(balance>=suma){
				balance=balance-suma;
			}
		setChanged();
		notifyObservers(initial);
	}
	public String getTip() {
		return tip;
	}
	public void setTip(String tip) {
		this.tip = tip;
	}
	
	
}
