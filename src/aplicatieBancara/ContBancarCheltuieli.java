package aplicatieBancara;



@SuppressWarnings("serial")
public class ContBancarCheltuieli extends ContBancar  {

	private int idAcc; 
	private int idHolder;
	private int balance;
	private String tip;
	
	public ContBancarCheltuieli(int idAcc, int idHolder, int balance,String tip) {
		super(idAcc,idHolder,balance,tip);
		this.idAcc=idAcc;
		this.idHolder=idHolder;
		this.balance=balance;
		this.setTip(tip);
	}

	public void setIdHolder(int idHolder) {
		this.idHolder = idHolder;
	}
	
	public int getHolder(){
		return idHolder;
	}
	 public void resetValue() {
	 
	      clearChanged();
	   }
	public int getIdAcc() {
		return idAcc;
	}
	
	public int getSold() {
		return balance;
	}

	public void deposit(int suma) {
		int initial=balance;
		balance=balance+suma;
		setChanged();  //se seteaza obiectul ContBancarCheltuieli ca fiind "schimbat"=>s-a produs o modificare care
		               //necesita a fi observata 
		notifyObservers(initial); //se lanseaza o notificare care va "trezi" metoda update din clasa Client
	}

	public void withdraw(int suma) {
		int initial=balance;
		if(balance>=suma){
			balance=balance-suma;
		}
		setChanged();
		notifyObservers(initial); // se va transmite ca argument in functia update valoarea init pentru afisarea
	}                             //soldului inainte de retragere 

	public String getTip() {
		return tip;
	}

	public void setTip(String tip) {
		this.tip = tip;
	}
	
}