package aplicatieBancara;

import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.io.FileInputStream;
import javax.swing.JTable;
import java.lang.reflect.Field;

public class Banca implements BancaProc {
	
	private HashMap<Client,Set<ContBancar>> hashMap;
	private int numarClientiBanca;
	
	public Banca(){
		hashMap= new HashMap<Client,Set<ContBancar>>();	 
		readData();
	}
	
	public void addClient(Client client) {  
		assert client!=null;
		
		numarClientiBanca=numarClientiBanca+1;
		hashMap.put(client, new HashSet<ContBancar>());
		writeData();		
	}
	public void addAccount(Client client,ContBancar cont) {
		assert client!=null;
		assert cont!=null;
		
		cont.addObserver(client); //i se adauga contului asociat clientului ,un observator de tip client
		
		Set<ContBancar> setConturiCurente= hashMap.get(client);//se extrage setul de conturi de la "cheia" client
		setConturiCurente.add(cont); //la setul de conturi ale clientului se adauga noul cont 
		
		hashMap.remove(client);//se sterge vechiul hashing cu vechiul set de conturi 
		hashMap.put(client,setConturiCurente); //se adauga noile date in tabela de hashing
		writeData();
		
		
	}
    public Client getClient(int id){
		assert id>=0;
		for(Client clientCurent: hashMap.keySet()){ //se parcurg toti clientii ,in hashMap ,clientii sunt cheile
			if(clientCurent.getId()==id)  //daca clientul cautat in lista are id-ul identic cu cel dorit 
				return clientCurent;     // atunci se returneaza clientul din hashMap
		}
		return null;
	}
	public void editClient(Client client, String name, String phone, String mail) {
		assert client!=null;
		//se creeaza un client nou,singurele date care raman de la vechiul client sunt id-ul si CNP-ul
		Client clientNou = new Client(client.getId(),name,client.getCnp(),phone,mail);
		hashMap.put(clientNou, hashMap.get(client));
		hashMap.remove(client);
		writeData();
	
		
	}
    public void stergeClient(Client client) {
		assert hashMap.isEmpty()==false; 
		
		hashMap.remove(client);
		numarClientiBanca=numarClientiBanca-1;
		writeData();
		
	}
	public void editCont(int idDetinator,int idCont, int idDetinatorNou) {
		Client client = getClient(idDetinatorNou); // se cauta in hashMap clientul cu noul id
		ArrayList<ContBancar> seturiConturi= getAccounts(idDetinator);//se extrag seturile de conturi pentru client
		ContBancar contGasit = null;
		for(ContBancar a: seturiConturi)
			if(a.getIdAcc()==idCont)
				contGasit=a;
		
		stergeCont(idCont,idDetinator);	
		contGasit.setIdHolder(idDetinatorNou);
		addAccount(client,contGasit);
		writeData();
	
	
	}
	public void stergeCont(int idCont,int idDetinator) {
		assert hashMap.isEmpty()==false;
		
		ContBancar contGasit =null;
		for(Client clientCurent: hashMap.keySet()){//se parcurg toti clientii
		    	for(Set<ContBancar> la: hashMap.values()){ //se parcurg toate seturile de conturi
		    		for(ContBancar a: la){//pentru fiecare set de cont se verifica daca se gaseste contul cu id-ul 
		    			if(a.getIdAcc()== idCont )//dorit a se sterge si daca 
		    				if(idDetinator==clientCurent.getId()) //detinatorul coincide cu al doilea param
		    				                                                
		    				 contGasit=a; 
		    		}
		    		la.remove(contGasit);
		    	}
		    }
		
		writeData();
		
	}
	public ArrayList<ContBancar> getAccounts(int idClient){ //se extrag conturile unui anumit client
		assert idClient>=0;
		ArrayList<ContBancar> conturiClient= new ArrayList<ContBancar>();
		for(Client clientCurent: hashMap.keySet()){
		  if(clientCurent.getId() == idClient){
			for(Set<ContBancar> la: hashMap.values()){//se parcurg seturile de conturi  
			 for(ContBancar a: la)//pentru fiecare set de cont se verifica daca 
				if(a.getHolder()==clientCurent.getId()) //detinatorul contului curent este egal cu cel dorit
					conturiClient.add(a);
				}
			}
	    }
		return conturiClient;
	}
	public ArrayList<Client> getClients(){
		ArrayList<Client> clienti= new ArrayList<Client>();
		for(Client p: hashMap.keySet()){
		    clienti.add(p);
		}
		return clienti;
	}
	public ArrayList<ContBancar> getAllAccounts(){
		ArrayList<ContBancar> listaConturi= new ArrayList<ContBancar>();
	    	for(Set<ContBancar> la: hashMap.values())
	    		for(ContBancar a: la)
	    				listaConturi.add(a);
		return listaConturi;
	}
	public JTable createTable(ArrayList<Object> objects){
		int i=0,k=0;
		Object obiectCurent= objects.get(0);
		Object[] headers;
		
		if(obiectCurent instanceof Client)
			headers= new Object[obiectCurent.getClass().getDeclaredFields().length];
		else
			headers= new Object[obiectCurent.getClass().getSuperclass().getDeclaredFields().length];
		
		for (Field field : obiectCurent.getClass().getDeclaredFields()) {
			try {
				field.setAccessible(true); 
				headers[i]=field.getName();
				i++;

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	
		Object[][] data= new Object[objects.size()][headers.length];
		
		for(int j=0;j<objects.size();j++){
			k=0;
			for (Field field : objects.get(j).getClass().getDeclaredFields()) {
				
				try {
					field.setAccessible(true);
					data[j][k] = field.get(objects.get(j));
					k++;

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		
		JTable table= new JTable(data,headers);
		
		return table;
	}
	
	@SuppressWarnings("unchecked")
	public void readData() {
		 try {
	         FileInputStream fileIn = new FileInputStream("bank.ser");
	         ObjectInputStream in = new ObjectInputStream(fileIn);
	         
	         hashMap = ((HashMap<Client, Set<ContBancar>>) in.readObject());
	         in.close();
	         fileIn.close();
	        
		 }catch(Exception i) {
	         i.printStackTrace();
	         return;
	      }
	}

	public void writeData() {
		try{
		FileOutputStream fileOut = new FileOutputStream("bank.ser");
	    ObjectOutputStream out = new ObjectOutputStream(fileOut);

	    out.writeObject(hashMap);
	    out.close();
	    fileOut.close();
	 
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public boolean isWellFormed(){
		if(numarClientiBanca==hashMap.size())
			return true;
		return false;
	}

}