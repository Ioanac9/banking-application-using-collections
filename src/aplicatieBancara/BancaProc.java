package aplicatieBancara;



public interface BancaProc {
	public void stergeClient(Client person);
	public void addClient(Client person);
	public void editClient(Client person, String name, String phone,String mail);
	public void stergeCont(int id,int idHolder);
	public void addAccount(Client person,ContBancar account) ;
	public void editCont(int idH,int idA, int idNewH) ;
	public void readData() ;
	public void writeData() ;
}
