package aplicatieBancara;

import static org.junit.Assert.*;
import org.junit.Test ;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;


public class Testare{
	
	HashMap<Client,Set<ContBancar>> hashMap;
	 Client ana;
	 Set<ContBancar> cont1;
	 Client ioana;
	 Set<ContBancar> cont2;
	 Client daniela; 
	 Set<ContBancar> cont3;
	
	public void aplicatieTest(){
		hashMap = new HashMap<Client,Set<ContBancar>>();
		cont1= new HashSet<ContBancar>();
		cont2= new HashSet<ContBancar>();
		
	    ana= new Client(1,"Ana Popescu","2231555","0766512668","ana97@yahoo.com");
	    ioana= new Client(2,"Ioana Popescu","229871","0789876524","ioana97@gmail.com");
	   
		cont1.add(new ContBancarCheltuieli(10,198,2000, "Spending Account"));
		cont1.add(new ContBancarEconomii(16,198,7000,"Saving Account"));
		
		cont2.add(new ContBancarCheltuieli(1,100,5000,"Spending Account"));
		cont2.add(new ContBancarEconomii(2,100,5000,"Saving Account"));
		
		hashMap.put(ana,cont1);
		hashMap.put(ioana,cont2);
	}
	@Test
	public void addClient(){
		aplicatieTest();
		cont3= new HashSet<ContBancar>();
		daniela= new Client(2,"Daniela Popescu","299871","0789876524","daniela97@gmail.com");
		
		cont3.add(new ContBancarCheltuieli(1,100,5000,"Spending Account"));
		cont3.add(new ContBancarEconomii(2,100,5000,"Saving Account"));
		hashMap.put(daniela, cont3);
		assertEquals(3,hashMap.size());
	}
	
	@Test
	public void addCont(){
		aplicatieTest();
		ContBancar a= new ContBancarCheltuieli(17,198,500,"Spending Account");
		cont1.add(a);
		hashMap.put(ana,cont1);
		
		assertEquals(3,cont1.size());
	}
	
	@Test
	public void removeClients(){
		aplicatieTest();
		hashMap.remove(ana);
      assertEquals(null,hashMap.get(ana));
	}
	
	@Test
	public void removeAllAccounts(){
		aplicatieTest();
		    	for(Set<ContBancar> s: hashMap.values()){  		
		    		s.removeAll(s);
		    	}
		
		assertEquals(0,cont1.size());
	}
	@Test
	public void editClient(){
		aplicatieTest();
		Client john= new Client(ioana.getId(),"John",ioana.getCnp(),"0754000341","john97@yahoo.com");
		
		hashMap.put(john,cont1);
		hashMap.remove(ioana);
		
		assertEquals(2,hashMap.size());
	}
	
}