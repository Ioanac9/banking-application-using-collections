package aplicatieBancara;

import java.io.IOException;
import java.io.Serializable;
import java.util.Observable;
import java.util.Observer;
import java.util.Random;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

@SuppressWarnings("serial")
public class Client implements Observer,Serializable { //implementeaza Observer deoarece obiectele de tip client 
	                                                  //sunt observatori ale clasei ContBancar 
	private int id;
	private String name;
	private String cnp;
	private String phone;
	private String mail;
	
	
	public Client(Integer id, String name,String cnp, String phone,String mail){
		this.id=id;
		this.name=name;
		this.cnp=cnp;
		this.phone=phone;
		this.mail=mail;
	}
	@Override
	public int hashCode(){
		int hashCode=Integer.parseInt(cnp)%320%100;
		return hashCode;
	}

	@Override
	public void update(Observable o, Object arg) {
		Random rand = new Random();
		int rand_int1 = rand.nextInt(1000);
		List<String> lines= Arrays.asList("Contul bancar numarul:"+((ContBancar)o).getIdAcc()+
				" al carui detinator este clientul cu numarul: " +((ContBancar)o).getHolder() +
				 " a avut sold-ul initial: "+(Integer)arg +" iar noul sold este: "+((ContBancar)o).getSold());
		/*se creeaza numele fisierului in care se va adauga chitanta*/
		Path file= Paths.get("cont"+((ContBancar)o).getIdAcc()+"Chitanta_"+rand_int1+".txt");
		try {                           
			Files.write(file, lines, Charset.forName("UTF-8"));
		} catch (IOException e) {
		    e.printStackTrace();
		}
		
	}
	public int getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public String getPhone() {
		return phone;
	}
	
	public String getMail() {
		return mail;
	}
	
	public String getCnp() {
		return cnp;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public void setMail(String mail) {
		this.mail = mail;
	}
}
